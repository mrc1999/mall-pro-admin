import instance from '@/utils/request'

export function getUmsMenuList () {
  return instance({
    method: 'GET',
    url: '/umsMenu/'
  })
}

export function getRoleMenuList (id) {
  return instance({
    method: 'GET',
    url: '/umsMenu/role/' + id
  })
}

export function updateRoleMenu (id, menu) {
  return instance({
    method: 'PUT',
    url: `/umsMenu/role/${id}`,
    data: menu
  })
}

export function getUmsMenuOne (id) {
  return instance({
    method: 'GET',
    url: `/umsMenu/${id}`
  })
}

export function saveUmsMenu (data) {
  return instance({
    method: 'POST',
    url: '/umsMenu/',
    data: data
  })
}

export function updateUmsMenu (data, id) {
  return instance({
    method: 'PUT',
    url: `/umsMenu/${id}`,
    data: data
  })
}

export function delUmsMenu (id) {
  return instance({
    method: 'DELETE',
    url: `/umsMenu/${id}`
  })
}

export function getUmsMenuPage (obj) {
  return instance({
    method: 'POST',
    url: '/umsMenu/page',
    data: obj
  })
}
