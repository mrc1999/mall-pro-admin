import instance from '@/utils/request'

export function getUmsRoleList () {
  return instance({
    method: 'GET',
    url: '/umsRole/'
  })
}

export function getUmsRoleOne (id) {
  return instance({
    method: 'GET',
    url: '/umsRole/' + id
  })
}

export function saveUmsRole (data) {
  return instance({
    method: 'POST',
    url: '/umsRole/',
    data: data
  })
}

export function updateUmsRole (data, id) {
  return instance({
    method: 'PUT',
    url: '/umsRole/' + id,
    data: data
  })
}

export function delUmsRole (id) {
  return instance({
    method: 'DELETE',
    url: '/umsRole/' + id
  })
}

export function getUmsRolePage (obj) {
  return instance({
    method: 'POST',
    url: '/umsRole/page',
    data: obj
  })
}
