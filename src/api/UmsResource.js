import instance from '@/utils/request/'

export function getRoleResourceList (id) {
  return instance({
    method: 'GET',
    url: `/umsResource/role/${id}`
  })
}

export function updateRoleResource (id, menu) {
  return instance({
    method: 'PUT',
    url: `/umsResource/role/${id}`,
    data: menu
  })
}
