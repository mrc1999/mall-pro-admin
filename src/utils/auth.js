import Cookies from 'js-cookie'

const TOKEN_VALUE = 'AUTH-TOKEN'
const TOKEN_KEY = 'TOKEN-KEY'

/**
 * 保存一个token 信息到缓存
 * @param token
 * @returns {*}
 */
export function setToken (token) {
  return Cookies.set(TOKEN_VALUE, token)
}

/**
 * 获取token,若不存在则返回空字符串
 * @returns {string|*}
 */
export function getToken () {
  const value = Cookies.get(TOKEN_VALUE)
  return value === undefined ? '' : value
}

/**
 * 保存一个token自定义头信息
 * @param head
 * @returns {*}
 */
export function setTokenKey (head) {
  return Cookies.set(TOKEN_KEY, head)
}

/**
 * 获取toKen header头信息，空则返回一个默认头
 * @returns {string|*}
 */
export function getTokenKey () {
  const val = Cookies.get(TOKEN_KEY)
  return val === undefined ? 'Authorization' : val
}
