import axios from 'axios'
import {Message} from 'element-ui'
import store from '@/store/'

// 创建axios实例
const service = axios.create({
  baseURL: 'http://localhost/', // 基础baseurl
  timeout: 15000 // 超时时间
})

// 请求拦截器，加入token 信息
service.interceptors.request.use((config) => {
  config.headers.common[store.getters.tokenKey] = store.getters.tokenValue
  return config
}, function (error) {
  return Promise.reject(error)
})

// 实现拦截器，处理code 非200的请求
service.interceptors.response.use(response => {
  const rest = response.data
  // code 非200 进行处理
  if (rest.code !== 200) {
    Message({
      message: rest.message,
      type: 'error',
      duration: 3 * 1000
    })
    throw new Error(rest.message)
  }
  return response.data
}, error => {
  // 处理异常信息
  console.log('error' + error)
  Message({
    message: error.message,
    type: 'error',
    duration: 3 * 1000
  })
  return Promise.reject(error)
})

export default service
