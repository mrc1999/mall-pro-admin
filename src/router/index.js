import Vue from 'vue'
import Router from 'vue-router'
import layout from '@/view/layout/'

Vue.use(Router)

// 系统路由
export const constantRouterMap = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/view/Login/'),
    hidden: true
  },
  {
    path: '/',
    redirect: '/home',
    meta: {title: '首页', icon: 'system'},
    component: layout,
    children: [
      {
        path: 'home',
        name: 'home',
        meta: {title: '首页', icon: 'system'},
        component: () => import('@/view/home/')
      }
    ]
  },
  {
    path: '/404',
    name: '404',
    component: () => import('@/view/404'),
    hidden: true
  }
]

// 动态菜单
export const asyncRouterMap = [
  {
    path: '/ums',
    redirect: '/ums/admin',
    name: 'ums',
    component: layout,
    meta: {title: '系统管理', icon: 'system'},
    children: [
      {
        path: 'admin',
        name: 'admin',
        component: () => import('@/view/ums/admin/'),
        meta: {title: '系统用户', icon: 'ums-admin'}
      },
      {
        path: 'role',
        name: 'role',
        component: () => import('@/view/ums/role/'),
        meta: {title: '系统角色', icon: 'ums-role'}
      },
      {
        path: 'menu',
        name: 'menu',
        component: () => import('@/view/ums/menu/'),
        meta: {title: '系统菜单', icon: 'ums-menu'}
      },
      {
        path: 'role/menu/:id',
        name: 'roleMenu',
        component: () => import('@/view/ums/role/menu/'),
        meta: {title: '角色配置', icon: 'ums-menu'},
        hidden: true
      }
    ]
  }
]
export default new Router({
  scrollBehavior: () => ({y: 0}),
  routes: constantRouterMap
})
