const getter = {
  tokenValue: (state) => state.user.tokenValue,
  roles: (state) => state.user.roles,
  username: (state) => state.user.username,
  tokenKey: (state) => state.user.tokenKey,
  addRouters: (state) => state.permission.addRouters,
  routers: (state) => state.permission.routers
}

export default getter
