import {setToken, getToken, setTokenKey, getTokenKey} from '@/utils/auth'
import {pullUserInfo} from '@/api/UmsAdmin/'

const app = {
  state: {
    tokenKey: getTokenKey(),
    tokenValue: getToken(),
    roles: [],
    username: ''
  },
  mutations: {
    SET_TOKEN: (state, token) => {
      setToken(token)
      state.tokenValue = token
    },
    SET_TOKEN_KEY: (state, val) => {
      setTokenKey(val)
      state.tokenKey = val
    },
    SET_ROLES: (state, val) => {
      state.roles = val
    },
    SET_USER_NAME: (state, val) => {
      state.username = val
    }
  },
  actions: {
    PullUserInfo ({commit, state}) {
      return new Promise(resolve => {
        pullUserInfo().then(res => {
          let data = res.data
          commit('SET_ROLES', data.roles)
          commit('SET_USER_NAME', data.username)
          resolve(data)
        })
      })
    }
  }
}

export default app
